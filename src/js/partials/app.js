$(document).ready(function(){
  // SLIDER
  var sliderHmBg = $('#hm-first-screen-bg').bxSlider({
    pager: false,
    controls: false
  });
  var sliderHmAction = $('#hm-actions-slider').bxSlider({
    pagerCustom: '#hm-actions-slider-pager',
    controls: false
  });
  var prevHmSlider = $('#hm-actions-slider-prev');
  var nextHmSlider = $('#hm-actions-slider-next');

  prevHmSlider.on('click', function(){
    sliderHmBg.goToPrevSlide();
    sliderHmAction.goToPrevSlide();
  });
  nextHmSlider.on('click', function(){
    sliderHmBg.goToNextSlide();
    sliderHmAction.goToNextSlide();
  });

  $(window).on('resize', function(){
    sliderHmAction.reloadSlider();
  });

  //MODAL
  var newsFlag = 0;
  var loadNewsModal = function(){
    $.ajax({
  	  url: "hm-news.html",
  	  cache: false
  	}).done(function( html ) {
  	  $("#modal-news-cont").append(html);
  	});
  }
  $('#modal-news').on({
    'show.uk.modal': function(){
        if (newsFlag == 0) {
          loadNewsModal();
          newsFlag = 1;
        }
    },

    'hide.uk.modal': function(){

    }
  })
});
