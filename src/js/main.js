/*
 * Third party
 */
//= ../../bower_components/modernizr/modernizr.js
//= ../../bower_components/jquery/dist/jquery.js
//= ../../bower_components/uikit/js/uikit.js
//= ../../bower_components/bxslider-4/dist/jquery.bxslider.min.js


/*
 * Custom
 */
//= partials/app.js
